open Unix

let port = ref 8989
and backlog = ref 3
and storage_path = ref "messages.xml"
and counter = ref 0  (* to generate the messages' indices,
                        it could as well be the server's time *)

type string_queue = (int * string * string) Queue.t

let queue: string_queue = Queue.create ()  (* messages are strings *)
and mutex = Mutex.create ()  (* because the queues aren't thread-safe *)

let expected_users = 16  (* TODO: should match backlog? *)
let users: (string, unit) Hashtbl.t = Hashtbl.create expected_users

(* The protocol is as follows:
   - client sends the nickname as the first sole line
   - server sends the separator as the first sole line
   - the order doesn't matter, these are sent in opposite directions
   - the separator string is used as a control sequence
     to tell the client when to start reading an input line,
     and serves as a prefix for various technical messages
   - e.g. the acknowledgment of reception (see below) *)

let generate_separator () =
  Int64.to_string (Random.int64 Int64.max_int)

let output_endline channel string =
  output_string channel string;
  output_string channel "\n";
  flush channel

let save_messages () =
  let xml_instruction name aa =
    let open String in
    let base = Xml.to_string (Xml.Element (name, aa, [])) in
    concat (sub base 1 (length base - 3)) ["<?"; "?>"]  (* "<", "/>" *)
  in
  let out = open_out !storage_path in
  output_endline out (xml_instruction "xml" ["version", "1.0";
                                             "encoding", "UTF-8"]);
  let messages =
    let message_to_xml_list rest (_, nick, text) =
      Xml.Element ("message", ["nick", nick], [Xml.PCData text]) :: rest
    in Queue.fold message_to_xml_list [] queue in
  output_endline out (Xml.to_string_fmt
                        (Xml.Element ("messages", [], messages)))

let insert_message delay nick text =
  Mutex.lock mutex;
  incr counter;
  Queue.push (!counter, nick, text) queue;
  Thread.delay delay;  (* artificial delay, for testing purposes *)
  Mutex.unlock mutex

let load_messages () =
  let load_message message_element =
    match message_element with
    | Xml.Element ("message", ["nick", nick], [Xml.PCData text]) ->
       insert_message 0. nick text
    | _ -> print_endline "Wrong storage format: message element!";
           exit 6 in
  match Xml.parse_file !storage_path with
  | Xml.Element ("messages", [], messages)
    -> List.iter load_message messages
  | _ -> print_endline "Wrong storage format: root element!"; exit 5

let conn_main s =
  let sep = generate_separator ()
  and cout = out_channel_of_descr s
  and cin = in_channel_of_descr s in
  let nick = input_line cin in  (* TODO: check that the nick is given *)
  let rec sync_messages starting_from =
    let send_message _ (i, u, m) =
      if starting_from < i then
        output_endline cout (String.concat "" [u; ": "; m]) else ();
      flush cout; i in

    (* send all the new messages out there to date *)
    let current = Queue.fold send_message starting_from queue in
    output_endline cout sep;

    (* add one more message to the queue, blocking *)
    let line = input_line cin in
    if line = "" then () else  (* empty lines aren't stored *)
      insert_message 0.5 nick line;  (* simulate a long operation *)

    (* immediate response, acknowledgment of reception *)
    output_string cout sep;
    output_endline cout " message received";

    (* continue from where we stopped last time *)
    sync_messages current in

  output_endline cout sep;  (* tell the client the control sequence *)
  begin
    match Hashtbl.find_opt users nick with
    | Some () -> output_endline cout "User exists!"; flush cout
    | None ->
       Hashtbl.add users nick ();
       try sync_messages 0  (* send them the whole history first *)
       with End_of_file -> Hashtbl.remove users nick
  end;

  (* we're done here *)
  print_endline "Closing the connection.";
  close s

let rec accept_loop sock =
  let (s, _) = accept sock in
  print_endline "Accepted a connection.";
  let _ = Thread.create conn_main s in
  accept_loop sock

let stop _ =
  Mutex.lock mutex;  (* the show is over for now *)
  print_endline "Dumping the messages...";
  save_messages ();
  exit 0  (* normal exit *)

let parse_options () =
  let constant x _ = x
  and specs = ["-port", Arg.Set_int port,
               "specifies the number of the port to serve from";
               "-store", Arg.Set_string storage_path,
               "specifies where to store the messages between the runs";
               "-backlog", Arg.Set_int backlog,
               "specifies what?..."] in
  Arg.parse specs (constant ())
    "Arguments: [-port <number>] [-store <path>] [-backlog <number>]"

let () =
  parse_options ();
  load_messages ();
  Random.self_init ();
  let sock = socket PF_INET6 SOCK_STREAM 0 in
  setsockopt sock SO_REUSEADDR true;
  bind sock (ADDR_INET (inet6_addr_any, !port));
 listen sock !backlog;
  print_endline "Entering accept loop...";
  let open Sys in
  List.iter (fun signal ->
      set_signal signal (Signal_handle stop)) [sigterm; sigint; sighup];
  accept_loop sock
